import java.io.File;
import java.io.IOException;
import java.util.List;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import entities.Player;

public class DataReader {

	public static List<Player> readPlayers() {

		ObjectMapper mapper = new ObjectMapper();
		List<Player> players = null;
		try {
			players = mapper.readValue(new File("src/data/players.json"), new TypeReference<List<Player>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return players;
	}

	public static void WritePlayers(List<Player> players) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(new File("src/data/players.json"), players);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
