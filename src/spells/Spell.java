package spells;

public class Spell {
	
	String name;
	String description;
	double baseDmg;
	double blood;
	double cooldown;
	double shield;
	
	public Spell(String name, String description, double baseDmg, double blood, double cooldown, double shield) {
		super();
		this.name = name;
		this.description = description;
		this.baseDmg = baseDmg;
		this.blood = blood;
		this.cooldown = cooldown;
		this.shield = shield;
	}
	
	
	/*GETTERS & SETTERS*/
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getBaseDmg() {
		return baseDmg;
	}
	public void setBaseDmg(double baseDmg) {
		this.baseDmg = baseDmg;
	}
	public double getBlood() {
		return blood;
	}
	public void setBlood(double blood) {
		this.blood = blood;
	}
	public double getCooldown() {
		return cooldown;
	}
	public void setCooldown(double cooldown) {
		this.cooldown = cooldown;
	}
	public double getShield() {
		return shield;
	}
	public void setShield(double shield) {
		this.shield = shield;
	}
	
	@Override
	public String toString() {
		return "Spell [name=" + name + ", description=" + description + ", baseDmg=" + baseDmg + ", blood=" + blood
				+ ", cooldown=" + cooldown + ", shield=" + shield + "]";
	}

	

}
