package entities;

import items.Equipment;
import items.Head;

public class Player implements Entity {

	// Level
	private int level = 1;
	
	private int MAXLEVEL;
	
	private double requiredExpToLvlUp;
	
	private String name;
	
	

	// Main Stats
	private double stamina, intelect, agility, strength, faith = 10 * level;
	// other Stats
	private double armor, dodge, parry, critic = 0;

	// internal Stats
	private double attackSpeed, movementSpeed = 1;
	private double attackPower = (10 * level);
	private double totalDefense = armor + stamina;
	private double hp = (100 + ((stamina / 2) * level));

	// GEAR
	Equipment head = new Head();// do same with others

	
	
	//GETTERS & SETTERS

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the stamina
	 */
	public double getStamina() {
		return stamina;
	}

	/**
	 * @param stamina the stamina to set
	 */
	public void setStamina(double stamina) {
		this.stamina = stamina;
	}

	/**
	 * @return the intelect
	 */
	public double getIntelect() {
		return intelect;
	}

	/**
	 * @param intelect the intelect to set
	 */
	public void setIntelect(double intelect) {
		this.intelect = intelect;
	}

	/**
	 * @return the agility
	 */
	public double getAgility() {
		return agility;
	}

	/**
	 * @param agility the agility to set
	 */
	public void setAgility(double agility) {
		this.agility = agility;
	}

	/**
	 * @return the strength
	 */
	public double getStrength() {
		return strength;
	}

	/**
	 * @param strength the strength to set
	 */
	public void setStrength(double strength) {
		this.strength = strength;
	}

	/**
	 * @return the faith
	 */
	public double getFaith() {
		return faith;
	}

	/**
	 * @param faith the faith to set
	 */
	public void setFaith(double faith) {
		this.faith = faith;
	}

	/**
	 * @return the armor
	 */
	public double getArmor() {
		return armor;
	}

	/**
	 * @param armor the armor to set
	 */
	public void setArmor(double armor) {
		this.armor = armor;
	}

	/**
	 * @return the dodge
	 */
	public double getDodge() {
		return dodge;
	}

	/**
	 * @param dodge the dodge to set
	 */
	public void setDodge(double dodge) {
		this.dodge = dodge;
	}

	/**
	 * @return the parry
	 */
	public double getParry() {
		return parry;
	}

	/**
	 * @param parry the parry to set
	 */
	public void setParry(double parry) {
		this.parry = parry;
	}

	/**
	 * @return the critic
	 */
	public double getCritic() {
		return critic;
	}

	/**
	 * @param critic the critic to set
	 */
	public void setCritic(double critic) {
		this.critic = critic;
	}

	/**
	 * @return the attackSpeed
	 */
	public double getAttackSpeed() {
		return attackSpeed;
	}

	/**
	 * @param attackSpeed the attackSpeed to set
	 */
	public void setAttackSpeed(double attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	/**
	 * @return the movementSpeed
	 */
	public double getMovementSpeed() {
		return movementSpeed;
	}

	/**
	 * @param movementSpeed the movementSpeed to set
	 */
	public void setMovementSpeed(double movementSpeed) {
		this.movementSpeed = movementSpeed;
	}

	/**
	 * @return the attackPower
	 */
	public double getAttackPower() {
		return attackPower;
	}

	/**
	 * @param attackPower the attackPower to set
	 */
	public void setAttackPower(double attackPower) {
		this.attackPower = attackPower;
	}

	/**
	 * @return the totalDefense
	 */
	public double getTotalDefense() {
		return totalDefense;
	}

	/**
	 * @param totalDefense the totalDefense to set
	 */
	public void setTotalDefense(double totalDefense) {
		this.totalDefense = totalDefense;
	}

	/**
	 * @return the hp
	 */
	public double getHp() {
		return hp;
	}

	/**
	 * @param hp the hp to set
	 */
	public void setHp(double hp) {
		this.hp = hp;
	}

	/**
	 * @return the head
	 */
	public Equipment getHead() {
		return head;
	}

	/**
	 * @param head the head to set
	 */
	public void setHead(Equipment head) {
		this.head = head;
	}


	/**
	 * @return the mAXLEVEL
	 */
	public int getMAXLEVEL() {
		return MAXLEVEL;
	}

	/**
	 * @param mAXLEVEL the mAXLEVEL to set
	 */
	public void setMAXLEVEL(int mAXLEVEL) {
		MAXLEVEL = mAXLEVEL;
	}

	/**
	 * @return the requiredExpToLvlUp
	 */
	public double getRequiredExpToLvlUp() {
		return requiredExpToLvlUp;
	}

	/**
	 * @param requiredExpToLvlUp the requiredExpToLvlUp to set
	 */
	public void setRequiredExpToLvlUp(double requiredExpToLvlUp) {
		this.requiredExpToLvlUp = requiredExpToLvlUp;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
