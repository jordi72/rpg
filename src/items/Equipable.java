package items;

public interface Equipable {
	
	public void equip();
	
	public void unEquip();

}
