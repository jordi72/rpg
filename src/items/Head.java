package items;

public class Head extends Equipment{
	
	private double stamina, intelect, agility, strength, faith, armor, dodge, parry, critic, hp;
	
	//Full Constructor
	public Head() {
		super();
	}

	public double getStamina() {
		return stamina;
	}

	public void setStamina(double stamina) {
		this.stamina = stamina;
	}

	public double getIntelect() {
		return intelect;
	}

	public void setIntelect(double intelect) {
		this.intelect = intelect;
	}

	public double getAgility() {
		return agility;
	}

	public void setAgility(double agility) {
		this.agility = agility;
	}

	public double getStrength() {
		return strength;
	}

	public void setStrength(double strength) {
		this.strength = strength;
	}

	public double getFaith() {
		return faith;
	}

	public void setFaith(double faith) {
		this.faith = faith;
	}

	public double getArmor() {
		return armor;
	}

	public void setArmor(double armor) {
		this.armor = armor;
	}

	public double getDodge() {
		return dodge;
	}

	public void setDodge(double dodge) {
		this.dodge = dodge;
	}

	public double getParry() {
		return parry;
	}

	public void setParry(double parry) {
		this.parry = parry;
	}

	public double getCritic() {
		return critic;
	}

	public void setCritic(double critic) {
		this.critic = critic;
	}

	public double getHp() {
		return hp;
	}

	public void setHp(double hp) {
		this.hp = hp;
	}

	@Override
	public String toString() {
		return "Head [stamina=" + stamina + ", intelect=" + intelect + ", agility=" + agility + ", strength=" + strength
				+ ", faith=" + faith + ", armor=" + armor + ", dodge=" + dodge + ", parry=" + parry + ", critic="
				+ critic + ", hp=" + hp + "]";
	}
}
