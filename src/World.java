import java.util.List;

import entities.Player;

public class World {
	
	private double baseExpToLvlUp = 500;
	
	private final int MAXLEVEL = 50;
	
	static List<Player> players = DataReader.readPlayers();
			
	public World() {
		for (Player player : players) {
			System.out.println(player.getName() + " " + player.getLevel());
			player.setMAXLEVEL(this.getMAXLEVEL());
			player.setRequiredExpToLvlUp(this.getBaseExpToLvlUp()*(Math.pow(player.getLevel(),1.1)));
		}
	}
	
	public static void shutdownWorld() {
		DataReader.WritePlayers(players);
	}
	
	
	//GETTERS & SETTERS

	/**
	 * @return the requiredExpToLvlUp
	 */
	public double getBaseExpToLvlUp() {
		return baseExpToLvlUp;
	}

	/**
	 * @param requiredExpToLvlUp the requiredExpToLvlUp to set
	 */
	public void setBaseExpToLvlUp(double baseExpToLvlUp) {
		this.baseExpToLvlUp = baseExpToLvlUp;
	}

	/**
	 * @return the mAXLEVEL
	 */
	public int getMAXLEVEL() {
		return MAXLEVEL;
	}
	
	

}
